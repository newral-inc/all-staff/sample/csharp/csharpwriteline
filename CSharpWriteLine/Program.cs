﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO; // StreamWriterを使用するため追加します。

namespace CSharpWriteLine
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter sw = null;
            try
            {
                // ファイルパス名を指定してStreamWriterを生成します。
                // パス名が無い場合、実行ファイルと同じフォルダ内にあるファイルが対象となります。
                string path = "sample.txt";
                sw = new StreamWriter(path);

                // 文字列をファイルに書き出します。
                sw.WriteLine("sample");

                // ファイルへの書き出しはバッファリングされ、任意のタイミングで書き出されます。
                // すぐに書き出したい場合はFlushメソッドを呼び出します。
                sw.Flush();
            }

            // ファイルに書き出せない場合など例外が発生します。
            catch (Exception e)
            {
                string message = e.Message;
                Console.WriteLine(message);
            }

            // StreamWriterの後始末を行います。
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
            }
        }
    }
}
